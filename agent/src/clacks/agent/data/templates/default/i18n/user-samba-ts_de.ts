<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>sambaSamAccount</name>
    <message>
        <location filename="../user-samba-ts.ui" line="14"/>
        <source>Samba-Terminalserver</source>
        <translation>Samba-Terminalserver</translation>
    </message>
    <message>
        <location filename="../user-samba-ts.ui" line="24"/>
        <source>Generic</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <location filename="../user-samba-ts.ui" line="32"/>
        <source>Home directory</source>
        <translation>Basisverzeichnis</translation>
    </message>
    <message>
        <location filename="../user-samba-ts.ui" line="59"/>
        <source>Profile path</source>
        <translation>Profil-Pfad</translation>
    </message>
    <message>
        <location filename="../user-samba-ts.ui" line="83"/>
        <source>Inherit client configuration</source>
        <translation>Client-Konfiguration übernehmen</translation>
    </message>
    <message>
        <location filename="../user-samba-ts.ui" line="90"/>
        <source>Initial program</source>
        <translation>Startprogramm</translation>
    </message>
    <message>
        <location filename="../user-samba-ts.ui" line="103"/>
        <source>Working directory</source>
        <translation>Arbeitsverzeichnis</translation>
    </message>
    <message>
        <location filename="../user-samba-ts.ui" line="121"/>
        <source>Connection</source>
        <translation>Verbindung</translation>
    </message>
    <message>
        <location filename="../user-samba-ts.ui" line="131"/>
        <source>Connection time limit</source>
        <translation>Verbindungszeitbeschränkung</translation>
    </message>
    <message>
        <location filename="../user-samba-ts.ui" line="144"/>
        <source>Disconnection time limit</source>
        <translation>Trennzeitbeschränkung</translation>
    </message>
    <message>
        <location filename="../user-samba-ts.ui" line="157"/>
        <source>IDLE time limit</source>
        <translation>Leerlaufzeitbeschränkung</translation>
    </message>
    <message>
        <location filename="../user-samba-ts.ui" line="190"/>
        <source>Shadowing</source>
        <translation>Spiegeln</translation>
    </message>
    <message>
        <location filename="../user-samba-ts.ui" line="203"/>
        <source>On broken or timed out</source>
        <translation>Bei Trennung oder abgelaufenem Zeitlimit</translation>
    </message>
    <message>
        <location filename="../user-samba-ts.ui" line="216"/>
        <source>Reconnect if disconnected</source>
        <translation>Wiederherstellen falls unterbrochen</translation>
    </message>
    <message>
        <location filename="../user-samba-ts.ui" line="242"/>
        <source>Connect client drives at log on</source>
        <translation>Client-Laufwerke beim Anmelden verbinden</translation>
    </message>
    <message>
        <location filename="../user-samba-ts.ui" line="249"/>
        <source>Connect client printers at log on</source>
        <translation>Client-Drucker beim Anmelden verbinden</translation>
    </message>
    <message>
        <location filename="../user-samba-ts.ui" line="256"/>
        <source>Default to main client printer</source>
        <translation>Standard-Drucker vom Client wählen</translation>
    </message>
    <message>
        <location filename="../user-samba-ts.ui" line="268"/>
        <source>Allow login on terminal server</source>
        <translation>Anmeldung am Terminalserver zulassen</translation>
    </message>
</context>
</TS>
