<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>posixAccount</name>
    <message>
        <location filename="../user-posix.ui" line="14"/>
        <source>POSIX</source>
        <translation>POSIX</translation>
    </message>
    <message>
        <location filename="../user-posix.ui" line="24"/>
        <source>Generic POSIX settings</source>
        <translation>Allgemeine POSIX-Einstellungen</translation>
    </message>
    <message>
        <location filename="../user-posix.ui" line="35"/>
        <source>Home directory</source>
        <translation>Benutzerverzeichnis</translation>
    </message>
    <message>
        <location filename="../user-posix.ui" line="48"/>
        <source>Login shell</source>
        <translation>Anmelde-Interpreter</translation>
    </message>
    <message>
        <location filename="../user-posix.ui" line="61"/>
        <source>GECOS</source>
        <translation>GECOS</translation>
    </message>
    <message>
        <location filename="../user-posix.ui" line="90"/>
        <source>Automatic UID/GID numbers</source>
        <translation>Automatische UID-/GID-Nummern</translation>
    </message>
    <message>
        <location filename="../user-posix.ui" line="97"/>
        <source>User ID</source>
        <translation>Benutzer-ID</translation>
    </message>
    <message>
        <location filename="../user-posix.ui" line="117"/>
        <source>Group ID</source>
        <translation>Gruppen-ID</translation>
    </message>
    <message>
        <location filename="../user-posix.ui" line="137"/>
        <source>Automatic GECOS</source>
        <translation>Automatischer GECOS</translation>
    </message>
    <message>
        <location filename="../user-posix.ui" line="165"/>
        <source>Group membership</source>
        <translation>Gruppenmitgliedschaft</translation>
    </message>
    <message>
        <location filename="../user-posix.ui" line="183"/>
        <source>Edit group membership</source>
        <translation>Gruppenmitgliedschaft bearbeiten</translation>
    </message>
    <message>
        <location filename="../user-posix.ui" line="187"/>
        <source>Name</source>
        <comment>cn</comment>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../user-posix.ui" line="192"/>
        <source>Description</source>
        <comment>description</comment>
        <translation>Beschreibung</translation>
    </message>
</context>
</TS>
